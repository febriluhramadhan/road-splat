﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuccessScript : MonoBehaviour
{
    public Button buttonNext;

    private void Awake()
    {
        buttonNext.onClick.RemoveAllListeners();
        buttonNext.onClick.AddListener(delegate { OnButtonNext(); });
    }

    void OnButtonNext()
    {
        Application.LoadLevel("main");
    }
}
