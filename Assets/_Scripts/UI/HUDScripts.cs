﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDScripts : MonoBehaviour {

    [Header("Button")]
    public Button buttonRight;
    public Button buttonLeft;
    public Button buttonRestart;

    [Header("Level Meter")]
    public Text textLevel;
    public Image fillImage;

    private WaitForSeconds delta;

    private void Awake()
    {
        delta = new WaitForSeconds(Time.deltaTime);

        buttonRight.onClick.RemoveAllListeners();
        buttonRight.onClick.AddListener(delegate { OnButtonRightClick(); });
        buttonLeft.onClick.RemoveAllListeners();
        buttonLeft.onClick.AddListener(delegate { OnButtonLeftClick(); });
        buttonRestart.onClick.RemoveAllListeners();
        buttonRestart.onClick.AddListener(delegate { OnButtonRestartClick(); });

        fillImage.fillAmount = 0;
    }
    
    void OnButtonRightClick()
    {
        //Debug.Log("RIGHT CLICK");
        //Application.LoadLevel(Application.loadedLevel);
    }

    void OnButtonLeftClick()
    {
        //Debug.Log("LEFT CLICK");
    }

    void OnButtonRestartClick()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void SetFillMeter(float amount)
    {
        //float value = amount / max;
        StartCoroutine(UpdateFillMeter(amount));
    }

    IEnumerator UpdateFillMeter(float value)
    {
        float step = (value - fillImage.fillAmount) / (1f/Time.deltaTime);
        while (fillImage.fillAmount < value)
        {
            fillImage.fillAmount += step;
            yield return delta;
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
