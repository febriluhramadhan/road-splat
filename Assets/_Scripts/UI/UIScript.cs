﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScript : MonoBehaviour
{
    public static UIScript instance;

    public MainMenuScript mainMenuScript;
    public HUDScripts hudScript;
    public GameOverScript gameOverScript;
    public SuccessScript successScript;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        ShowMainMenu();
    }

    public void ShowMainMenu()
    {
        mainMenuScript.gameObject.SetActive(true);
    }

    public void ShowHUD()
    {
        mainMenuScript.gameObject.SetActive(false);
        hudScript.gameObject.SetActive(true);
    }

    public void ShowGameOver()
    {
        hudScript.gameObject.SetActive(false);
        gameOverScript.gameObject.SetActive(true);
    }

    public void ShowSuccess()
    {
        hudScript.gameObject.SetActive(false);
        successScript.gameObject.SetActive(true);
    }

}
