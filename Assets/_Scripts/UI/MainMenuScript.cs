﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    public Button buttonTaptoPlay;

    private void Awake()
    {
        buttonTaptoPlay.onClick.RemoveAllListeners();
        buttonTaptoPlay.onClick.AddListener(delegate { PlayGame(); });
    }

    void PlayGame()
    {
        GameController.instance.ChangeState(GameState.GAMEPLAY);
    }
}
