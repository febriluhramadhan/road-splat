﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour
{
    public Button buttonRestart;

    private void Awake()
    {
        buttonRestart.onClick.RemoveAllListeners();
        buttonRestart.onClick.AddListener(delegate { OnButtonRestart(); });
    }

    void OnButtonRestart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
