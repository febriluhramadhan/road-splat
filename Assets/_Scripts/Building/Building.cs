﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public List<GameObject> listObject;

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < listObject.Count; i++)
        {
            listObject[i].SetActive(false);
        }
    }

    public void ShowAllObject()
    {
        StartCoroutine(UpdateShowAllObject());
        //for (int i = 0; i < listObject.Count; i++)
        //{
        //    listObject[i].SetActive(true);
        //}
    }

    IEnumerator UpdateShowAllObject()
    {
        float time = 0.5f;
        float deltaTime = time / (float)listObject.Count;
        WaitForSeconds delta = new WaitForSeconds(deltaTime);
        for (int i = 0; i < listObject.Count; i++)
        {
            listObject[i].SetActive(true);
            yield return delta;
        }
    }
}
