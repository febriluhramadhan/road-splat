﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleAcross : MonoBehaviour
{
    public Transform people;

    public float delayIdle;
    public float delayAcross;
    private float currentTime;

    private Vector3 startPoint;
    private Vector3 endPoint;
    
    private bool isSet = false;
    private bool isMove = false;

    private void Awake()
    {
    }

    public void SetPoint(Vector3 start, Vector3 end)
    {
        startPoint = start;
        endPoint = end;
        isSet = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isSet)
        {
            if (isMove && currentTime > delayIdle)
            {
                transform.position = Vector3.Lerp(startPoint, endPoint, Time.deltaTime);
            }
            else
            {
                currentTime += Time.deltaTime;
            }
        }
    }
}
