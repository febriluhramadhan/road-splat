﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : MonoBehaviour {

    public static RoadGenerator instance;
    [Header("Plane")]
    public GameObject planePrefab;
    [Space]
    [Header("Road")]
    public int roadAmount;
    public List<GameObject> roadPrefabs= new List<GameObject>();
    public List<Road> roadsSpawned = new List<Road>();
    [Space]
    [Header("Obstacle")]
    public GameObject obstaclePrefabs;

    [Space]
    public Cinemachine.CinemachineVirtualCamera vcam2;
    public GameObject plane;

    private int currIter = 0;
    private int maxIter = 1000;

    private int lastGridIndexX;
    private int lastGridIndexY;

    private Road lastRoad;

    public int minX;
    public int maxX;
    public int minY;
    public int maxY;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        
        lastGridIndexX = GridGenerator.instance.widthSize / 2;
        lastGridIndexY = GridGenerator.instance.heightSize / 2;
        //lastGridIndexY = 0;

        minX = lastGridIndexX;
        maxX = lastGridIndexX;
        minY = lastGridIndexY;
        maxY = lastGridIndexY;

        GenerateRoad();
    }

    // Use this for initialization
    void Start () {
        SpawnPlane();
	}

    void GenerateRoad()
    {
        for (int i = 0; i < roadAmount; i++)
        {
            if (i == 0)
            {
                lastRoad = Instantiate(roadPrefabs[0],
                    GridGenerator.instance.grids[lastGridIndexX, lastGridIndexY].transform.position,
                    Quaternion.identity).GetComponent<Road>();

                lastRoad.gameObject.name = roadPrefabs[0].name;
                GridGenerator.instance.grids[lastGridIndexX, lastGridIndexY].occupant = lastRoad;
                roadsSpawned.Add(lastRoad);

                lastRoad.transform.parent = transform;

                lastGridIndexY++;
            }
            else
            {   
                bool isValid = false;
                while (!isValid)
                {
                    currIter++;
                    if (currIter > maxIter)
                    {
                        Debug.Log("Max Iter "+roadsSpawned.Count+"/"+roadAmount);
                        //Application.Quit();
                        return;
                    }

                    int randNumber = Random.Range(0, roadPrefabs.Count);
                    Road road = roadPrefabs[randNumber].GetComponent<Road>();

                    if (lastRoad.endDirection != road.startDirection
                        || lastRoad.gameObject.name.Equals(roadPrefabs[randNumber].name) // for different road every spawn
                        )
                        continue;

                    Vector2 nextIndex = road.NextGridIndex(lastGridIndexX, lastGridIndexY);
                    bool isNextValid = GridGenerator.instance.isValid((int)nextIndex.x, (int)nextIndex.y);
                    bool isCurrentValid = GridGenerator.instance.isValid(lastGridIndexX, lastGridIndexY);

                    if (isNextValid && isCurrentValid)
                    {
                        isValid = true;

                        lastRoad = Instantiate(roadPrefabs[randNumber],
                            GridGenerator.instance.grids[lastGridIndexX, lastGridIndexY].transform.position,
                            roadPrefabs[randNumber].transform.rotation).GetComponent<Road>();

                        lastRoad.gameObject.name = roadPrefabs[randNumber].name;
                        GridGenerator.instance.grids[lastGridIndexX, lastGridIndexY].occupant = lastRoad;
                        roadsSpawned.Add(lastRoad);

                        //SpawnObstacle
                        //if (Random.Range(0, 100) < 20)
                        //{
                        //    SpawnObstacle(lastRoad, 1);
                        //}

                        lastRoad.transform.parent = transform;

                        minX = Mathf.Min(minX, lastGridIndexX);
                        minY = Mathf.Min(minY, lastGridIndexY);
                        maxX = Mathf.Max(maxX, lastGridIndexX);
                        maxY = Mathf.Max(maxY, lastGridIndexY);

                        lastGridIndexX = (int)nextIndex.x;
                        lastGridIndexY = (int)nextIndex.y;
                    }
                }
            }
        }
    }

    public void ShowAllRoad()
    {
        StartCoroutine(UpdateShowAllRoad());
    }

    IEnumerator UpdateShowAllRoad()
    {
        float time = 3;
        float deltaTime = time / (float)roadsSpawned.Count;
        WaitForSeconds delta = new WaitForSeconds(deltaTime);
        for (int i = 0; i < roadsSpawned.Count; i++)
        {
            roadsSpawned[i].ShowRoad();
            yield return delta;
        }
    }

    void SpawnObstacle(Road road,int amount)
    {
        int randNumber = Random.Range(-1, 2);
        int randIndex = Random.Range(0, road.pointsTransform.Count);
        Vector3 pos = Vector3.up * 0.5f;
        if (randNumber == 0)
        {
            pos += road.pointsTransform[randIndex].position;
        }
        else if (randNumber == -1)
        {
            pos += road.leftPointsTransform[randIndex].position;
        }
        else if (randNumber == 1)
        {
            pos += road.rightPointsTransform[randIndex].position;
        }
        Instantiate(obstaclePrefabs, pos, Quaternion.identity);
    }

    void SpawnPlane()
    {
        plane = Instantiate(planePrefab);
        //float posValue = (GridGenerator.instance.widthSize - 1) * (GridGenerator.instance.offsetX / 2);
        //float scaleValue = (float)GridGenerator.instance.widthSize / 2;
        int sizeX = maxX - minX;
        int sizeY = maxY - minY;
        Debug.Log("X " + minX + " " + maxX);
        Debug.Log("Y " + minY + " " + maxY);
        Debug.Log("SIZE "+sizeX+" "+sizeY);
        
        float scaleValueX = (float)(sizeX + 1) / 2;
        float scaleValueY = (float)(sizeY + 1) / 2;
        Debug.Log("SCALE " + scaleValueX + " " + scaleValueY);
        float posValueX = (minX + ((float)sizeX / 2)) * (GridGenerator.instance.offsetX);
        float posValueY = (minY + ((float)sizeY / 2)) * (GridGenerator.instance.offsetY);
        Debug.Log("POS " + posValueX + " " + posValueY);

        plane.transform.position = new Vector3(posValueX, plane.transform.position.y, posValueY);
        //plane.transform.localScale = new Vector3(scaleValueX, plane.transform.localScale.y, scaleValueY);
        plane.transform.localScale = new Vector3(scaleValueX + 0.1f, plane.transform.localScale.y, scaleValueY + 0.1f);

        vcam2.Follow = plane.transform;
        vcam2.LookAt = plane.transform;

        vcam2.m_Lens.OrthographicSize = ((scaleValueX + scaleValueY) / 2) * 7.7f * ((float)Screen.height / (float)Screen.width);//Mathf.Max(scaleValueX, scaleValueY) * 10;
        //vcam2.gameObject.SetActive(true);
    }
}
