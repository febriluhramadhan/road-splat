﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGenerator : MonoBehaviour
{
    public static BuildingGenerator instance;

    public List<GameObject> buildingPrefab;
    public List<Building> buildingSpawned = new List<Building>();

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        GenerateBuilding();
    }

    void GenerateBuilding()
    {
        for (int x = RoadGenerator.instance.minX; x <= RoadGenerator.instance.maxX; x++)
        {
            for (int y = RoadGenerator.instance.minY; y <= RoadGenerator.instance.maxY; y++)
            {
                if (GridGenerator.instance.grids[x, y].occupant == null)
                {
                    Building building = Instantiate(buildingPrefab[Random.Range(0,buildingPrefab.Count)], GridGenerator.instance.grids[x, y].transform.position, Quaternion.identity).GetComponent<Building>();
                    building.transform.parent = transform;
                    buildingSpawned.Add(building);
                }
            }
        }
    }

    public void ShowAllBuilding()
    {
        StartCoroutine(UpdateShowAllBuilding());
    }

    IEnumerator UpdateShowAllBuilding()
    {
        float time = 2;
        float deltaTime = time / (float)buildingSpawned.Count;
        WaitForSeconds delta = new WaitForSeconds(deltaTime);
        for (int i = 0; i < buildingSpawned.Count; i++)
        {
            buildingSpawned[i].ShowAllObject();
            yield return delta;
        }
    }
}
