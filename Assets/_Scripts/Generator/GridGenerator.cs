﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour {

    public static GridGenerator instance;

    public int widthSize;
    public int heightSize;
    public float offsetX;
    public float offsetY;

    public Vector3 startPos;
    
    public GameObject gridPrefab;

    public GridClass[,] grids;

    private Vector3 lastPos;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        //Random.seed = 1;
        grids = new GridClass[widthSize, heightSize];
        lastPos = startPos;
        GenerateGrid();
    }

    private void Start()
    {
        
    }

    void GenerateGrid()
    {
        for (int x = 0; x < widthSize; x++)
        {
            lastPos.z = startPos.z;
            for (int y = 0; y < heightSize; y++)
            {
                GridClass grid = Instantiate(gridPrefab, lastPos, Quaternion.identity).GetComponent<GridClass>();
                grids[x, y] = grid;
                grid.transform.parent = transform;
                grid.name = "" + gridPrefab.name + x + y;
                lastPos.z += offsetY;
            }
            lastPos.x += offsetX;
        }
    }

    public bool isValid(int x, int y)
    {
        return x >= 0 && x < widthSize &&
            y >= 0 && y < heightSize &&
            grids[x,y].occupant == null;
    }

}
