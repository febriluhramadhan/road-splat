﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController instance;

    [Header("Game Properties")]
    public GameState currentState;
    public GameState lastState;

    [Header("Point")]
    public List<Transform> allCenterPointsTransform = new List<Transform>();
    public List<Vector3> allCenterPoints = new List<Vector3>();
    public List<Transform> allRightPointsTransform = new List<Transform>();
    public List<Vector3> allRightPoints = new List<Vector3>();
    public List<Transform> allLeftPointsTransform = new List<Transform>();
    public List<Vector3> allLeftPoints = new List<Vector3>();
    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        GetAllPoint();
        GenerateCenterPoint();
    }

    private void Start()
    {
        Application.targetFrameRate = 60;
    }

    public void ChangeState(GameState state)
    {
        if (currentState == state) return;
        
        lastState = currentState;
        currentState = state;
        
        switch (state)
        {
            case GameState.MAINMENU:
                break;
            case GameState.PREPARE:
                break;
            case GameState.GAMEPLAY:
                UIScript.instance.ShowHUD();
                ColorCalculator.instance.StartCalculate();
                break;
            case GameState.GAMEOVER:
                UIScript.instance.ShowGameOver();
                break;
            case GameState.SUCCESS:
                RoadGenerator.instance.vcam2.gameObject.SetActive(true);
                RoadGenerator.instance.ShowAllRoad();
                BuildingGenerator.instance.ShowAllBuilding();
                UIScript.instance.ShowSuccess();
                break;
        }
    }
    
    void GetAllPoint()
    {
        List<Road> spawnedRoad = RoadGenerator.instance.roadsSpawned;
        for (int i = 0;i< spawnedRoad.Count;i++)
        {
            int maxIter;
            if (i == spawnedRoad.Count - 1)
            {
                maxIter = spawnedRoad[i].pointsTransform.Count;
            }
            else
            {
                maxIter = spawnedRoad[i].pointsTransform.Count - 1;
            }
            //allPoints.AddRange(r.points);
            for (int j = 0; j < maxIter; j++)
            {
                //if (!allCenterPointsTransform.Exists(tr => tr.position.Equals(t.position)))
                //{
                allCenterPointsTransform.Add(spawnedRoad[i].pointsTransform[j]);
                //}
            }

            for (int j = 0; j < maxIter; j++)
            {
                //if (!allRightPointsTransform.Exists(tr => tr.position.Equals(t.position)))
                //{
                allRightPointsTransform.Add(spawnedRoad[i].rightPointsTransform[j]);
                //}
            }

            for (int j = 0; j < maxIter; j++)
            {
                //if (!allLeftPointsTransform.Exists(tr => tr.position.Equals(t.position)))
                //{
                allLeftPointsTransform.Add(spawnedRoad[i].leftPointsTransform[j]);
                //}
            }

        }
    }

    void GenerateCenterPoint()
    {
        for (int i = 0; i < allCenterPointsTransform.Count; i++)
        {
            if (i + 1 < allCenterPointsTransform.Count)
            {
                allCenterPoints.AddRange(GeneratePointFromPoint(allCenterPointsTransform[i].position, allCenterPointsTransform[i + 1].position, 5f));
            }
            else
            {
                allCenterPoints.Add(allCenterPointsTransform[allCenterPointsTransform.Count-1].position);
            }
        }

        for (int i = 0; i < allRightPointsTransform.Count; i++)
        {
            if (i + 1 < allRightPointsTransform.Count)
            {
                allRightPoints.AddRange(GeneratePointFromPoint(allRightPointsTransform[i].position, allRightPointsTransform[i + 1].position, 5f));
            }
            else
            {
                allRightPoints.Add(allRightPointsTransform[allRightPointsTransform.Count - 1].position);
            }
        }

        for (int i = 0; i < allLeftPointsTransform.Count; i++)
        {
            if (i + 1 < allLeftPointsTransform.Count)
            {
                allLeftPoints.AddRange(GeneratePointFromPoint(allLeftPointsTransform[i].position, allLeftPointsTransform[i + 1].position, 5f));
            }
            else
            {
                allLeftPoints.Add(allLeftPointsTransform[allLeftPointsTransform.Count - 1].position);
            }
        }
    }

    List<Vector3> GeneratePointFromPoint(Vector3 current, Vector3 target,float amount)
    {
        float time = 0;
        float step = 1f / amount;
        List<Vector3> points = new List<Vector3>();
        while (time < 1)
        {
            Vector3 pos = Vector3.Lerp(current, target, time);
            points.Add(pos);
            time += step;
        }
        return points;
    }

    private void OnDrawGizmos()
    {
        //if (allCenterPointsTransform.Count > 0)
        //{
        //    Gizmos.color = Color.blue;
        //    foreach (Transform t in allCenterPointsTransform)
        //    {
        //        Gizmos.DrawWireSphere(t.position, 0.5f);
        //    }

        //    Gizmos.color = Color.yellow;
        //    foreach (Vector3 v in allCenterPoints)
        //    {
        //        Gizmos.DrawWireSphere(v, 0.4f);
        //    }

        //    Gizmos.color = Color.green;
        //    foreach (Vector3 v in allRightPoints)
        //    {
        //        Gizmos.DrawWireSphere(v, 0.4f);
        //    }

        //    Gizmos.color = Color.grey;
        //    foreach (Vector3 v in allLeftPoints)
        //    {
        //        Gizmos.DrawWireSphere(v, 0.4f);
        //    }
        //}
    }
}
