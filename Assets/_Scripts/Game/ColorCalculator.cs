﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorCalculator : MonoBehaviour
{
    public static ColorCalculator instance;

    public Camera renderCamera;
    public RenderTexture renderTextureArea;

    //private RenderTexture renderTexture;
    private Texture2D textureArea;
    private Color[] color;

    //public List<Color> colors = new List<Color>();
    //public List<int> colorsCount = new List<int>();

    [Header("Area Properties")]
    [Range(0,1)]
    public float minPercentAreToWin;
    public Color colorArea;
    public int colorAreaCount;
    public int maxAreaAmount;

    private bool initDataArea;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        initDataArea = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        //renderTexture = new RenderTexture(renderTextureArea);
        textureArea = new Texture2D(renderTextureArea.width, renderTextureArea.height, TextureFormat.ARGB32, false);
        renderCamera.transform.position = RoadGenerator.instance.plane.transform.position + Vector3.up;
        renderCamera.orthographicSize = Mathf.Max(RoadGenerator.instance.plane.transform.localScale.x, RoadGenerator.instance.plane.transform.localScale.z) * 5;
    }

    public void StartCalculate()
    {
        StartCoroutine(UpdateRenderTexture());
    }

    IEnumerator UpdateRenderTexture()
    {
        while (GameController.instance.currentState == GameState.GAMEPLAY)
        {
            CreateAtlasTexture();

            colorAreaCount = 0;

            color = textureArea.GetPixels();
            for (int i = 0; i < color.Length; i++)
            {
                color[i].a = 1f;

                if (color[i].Equals(colorArea))
                {
                    colorAreaCount += 1;

                }
            }

            if (!initDataArea)
            {
                initDataArea = true;
                maxAreaAmount = colorAreaCount;
            }
            else
            {
                int diff = maxAreaAmount - colorAreaCount;
                float fillAmount = (float)diff / (float)maxAreaAmount;
                UIScript.instance.hudScript.SetFillMeter(fillAmount);

                if (fillAmount >= minPercentAreToWin)
                {
                    GameController.instance.ChangeState(GameState.SUCCESS);
                }
            }

            //foreach (Color c in colors)
            //{
            //    float area = ((float)colorsCount[colors.IndexOf(c)] * 100 / (float)(renderTextureArea.width * renderTextureArea.height));
            //    //GameController.instance.AssignScore(c, area);
            //}

            //GameController.instance.UpdateScoreList();
            yield return new WaitForSeconds(1f);
        }
    }

    private void CreateAtlasTexture()
    {
        //renderCamera.aspect = 1.0f;
        //renderCamera.targetTexture = renderTextureArea;
        //renderCamera.Render();
        RenderTexture.active = renderTextureArea;
        textureArea.ReadPixels(new Rect(0.0f, 0.0f, renderTextureArea.width, renderTextureArea.height), 0, 0);
        RenderTexture.active = null;
        //renderCamera.targetTexture = null;
        textureArea.Apply();
    }
}
