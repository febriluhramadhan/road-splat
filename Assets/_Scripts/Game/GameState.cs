﻿public enum GameState
{
    MAINMENU,
    PREPARE,
    GAMEPLAY,
    GAMEOVER,
    SUCCESS,
    STATECOUNT
}
