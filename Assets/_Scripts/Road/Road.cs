﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Road : MonoBehaviour {

    public List<Transform> pointsTransform;
    public List<Transform> rightPointsTransform;
    public List<Transform> leftPointsTransform;

    public RoadDirection startDirection;
    public RoadDirection endDirection;

    public List<Vector3> centerPoints;

    public GameObject roadModel;

    private void Awake()
    {
        roadModel.SetActive(false);
    }

    public void ShowRoad()
    {
        roadModel.SetActive(true);
    }

    public Vector2 NextGridIndex(int x,int y)
    {
        Vector2 nextIndex = new Vector2(x, y);
        switch (endDirection)
        {
            case RoadDirection.UP:
                nextIndex.y--;
                break;
            case RoadDirection.DOWN:
                nextIndex.y++;
                break;
            case RoadDirection.RIGHT:
                nextIndex.x--;
                break;
            case RoadDirection.LEFT:
                nextIndex.x++;
                break;
        }
        return nextIndex;
    }
}
