﻿/*
 * This code is part of Arcade Car Physics for Unity by Saarg (2018)
 * 
 * This is distributed under the MIT Licence (see LICENSE.md for details)
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public struct MeshData
{
    public Vector3[] vert;
    public Vector2[] uv;
    public int[] tris;
}

// Created by Edward Kay-Coles a.k.a Hoeloe
public class Trail : MonoBehaviour
{
    private static int layerCount = 0;

    //Properties of the trail
    private float width;
    private Material m;
    private int rough;
    private int maxRough;

    //Parent object
    private Transform par;

    //Pieces for the mesh generation
    //private GameObject trail;
    private MeshFilter filter;
    private MeshRenderer render;
    private Mesh mesh;

    //Lists storing the mesh data
    private List<Vector3> verts = new List<Vector3>();
    private List<Vector2> uvs = new List<Vector2>();
    private List<int> tris = new List<int>();

    //Check if the trail is still being generated, and if it has completely faded
    private bool finished = false;

    private Vector3 previousPosition;
    private Vector3 currentPosition;
    public float minimumSegmentLength = 0.1f;
    private Vector3 positionOffset;
    
    //Set up the trail object, and parameters
    public void SetTrail(Transform parent, Material material, int roughness, Vector3 off, float wid)
    {
        maxRough = roughness;
        rough = 0;
        par = parent;
        width = wid;
        m = material;
        gameObject.layer = LayerMask.NameToLayer("Trail");
        filter = gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
        render = gameObject.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
        mesh = new Mesh();
        render.material = m;
        filter.mesh = mesh;

        render.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        render.receiveShadows = false;
        render.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
        render.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;

        render.sortingLayerName = "Trail";
        render.sortingOrder = layerCount++;
        //Debug.Log("LayerMask : " + layerCount);
        positionOffset = off;
    }

    //Call this when the trail should stop emitting
    public void Finish()
    {
        finished = true;
    }

    //Tells you if the trail is emitting or not
    public bool Finished
    {
        get { return finished; }
    }

    // Updates the state of the trail - Note: this must be called manually
    public void Update()
    {
        if (!finished) //Only add new segments if the trail is not being emitted
        {
            //Decides how often to generate new segments. Smaller roughness values are smoother, but more expensive
            if (rough > 0)
                rough--;
            else
            {
                rough = maxRough;

                Vector3 currentPosition = par.transform.position + positionOffset;
                if (Vector3.Distance(previousPosition, currentPosition) > minimumSegmentLength)
                {
                    previousPosition = currentPosition;
                    //Add new vertices as the current position
                    
                    Vector3 offset = par.right * width / 2f;
                    verts.Add(currentPosition - offset);
                    verts.Add(currentPosition + offset);
                    
                    uvs.Add(new Vector2(0, 1));
                    uvs.Add(new Vector2(1, 1));

                    //Don't try to draw the trail unless we have at least a rectangle
                    if (verts.Count < 3)
                        return;

                    //Add new triangles to the mesh
                    int c = verts.Count;
                    tris.Add(c - 1);
                    tris.Add(c - 2);
                    tris.Add(c - 3);
                    tris.Add(c - 3);
                    tris.Add(c - 2);
                    tris.Add(c - 4);

                    //Copy lists to arrays, ready to rebuild the mesh
                    Vector3[] v = new Vector3[c];
                    Vector2[] uv = new Vector2[c];
                    int[] t = new int[tris.Count];
                    verts.CopyTo(v, 0);
                    uvs.CopyTo(uv, 0);
                    tris.CopyTo(t, 0);

                    mesh.MarkDynamic();

                    mesh.vertices = v;
                    mesh.triangles = t;
                    mesh.uv = uv;
                }
            }
        }
    }

}
