﻿/*
 * This code is part of Arcade Car Physics for Unity by Saarg (2018)
 * 
 * This is distributed under the MIT Licence (see LICENSE.md for details)
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class TrailEmitter : MonoBehaviour
{
    [HideInInspector]
    public Trail trail;

    //Parameters
    public float width;
    public int roughness;

    public Material material;
    public bool trailing = false;

    public Transform parent;

    public Vector3 offset;
    public GameObject trailPrefab;

    public Transform trailParent;
    
    public bool Active
    {
        get { return (trail == null ? false : (!trail.Finished)); }
    }
    
    /// <summary>
    /// Creates a new trail.
    /// </summary>
    public void NewTrail()
    {
        if (!trailing)
        {
            EndTrail();
            trailing = true;
            trail = Instantiate(trailPrefab).GetComponent<Trail>();
            trail.SetTrail(parent, material, roughness, offset, width);
            trail.transform.parent = trailParent;
        }
    }

    /// <summary>
    /// Deactivate the last trail if it was already active.
    /// </summary>
    public void EndTrail()
    {
        if (trailing)
        {
            if (!Active) return;
            trail.Finish();
            trailing = false;
        }
    }

}