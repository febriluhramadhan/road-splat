﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour {

    public GameObject target;
    public Vector3 offset;

    private Vector3 startPos;
	// Use this for initialization
	void Start () {
        startPos = transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (target != null)
        {
            //startPos.x = target.transform.position.x;
            //startPos.y = target.transform.position.y;
            startPos = target.transform.position + offset;
            transform.position = startPos;
        }
	}
}
