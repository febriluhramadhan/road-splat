﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {

    public float speed;
    public float rotationSpeed;
    public float minDistanceNextIndex;

    [Header("Trail")]
    public bool activeOutLine;
    public TrailEmitter trailEmitter;
    public TrailEmitter trailEmitterRight;
    public TrailEmitter trailEmitterLeft;

    private LinePlace currLine = LinePlace.CENTER;
    private int currIndex;
    private bool isDie;
    private Rigidbody rigid;

    private float startPosY = 0.5f;
    private int dir;

    // Use this for initialization
    void Start()
    {
        dir = 1;
        currIndex = 0;
        transform.position = GameController.instance.allCenterPoints[currIndex] + (Vector3.up * startPosY);
        //Debug.Log(transform.position);
        isDie = false;
        rigid = GetComponent<Rigidbody>();

        CreateAllTrail();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        switch (GameController.instance.currentState)
        {
            case GameState.GAMEPLAY:
                Move();
                break;
            case GameState.SUCCESS:
                rigid.velocity = Vector3.zero;
                break;
        }
	}


    private void Move()
    {
        if (isDie)
            return;
        
        //Transform target = GameController.instance.allPoints[currIndex];

        Vector3 target;
        target.y = startPosY;

        switch (currLine)
        {
            case LinePlace.CENTER:
                target.x = GameController.instance.allCenterPoints[currIndex].x;
                target.z = GameController.instance.allCenterPoints[currIndex].z;
                break;
            case LinePlace.RIGHT:
                target.x = GameController.instance.allRightPoints[currIndex].x;
                target.z = GameController.instance.allRightPoints[currIndex].z;
                break;
            case LinePlace.LEFT:
                target.x = GameController.instance.allLeftPoints[currIndex].x;
                target.z = GameController.instance.allLeftPoints[currIndex].z;
                break;
            default:
                target.x = GameController.instance.allCenterPoints[currIndex].x;
                target.z = GameController.instance.allCenterPoints[currIndex].z;
                break;
        }


        //transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);

        Vector3 relativePos = (target - transform.position).normalized;
        float angle = Mathf.Atan2(relativePos.x, relativePos.z) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.AngleAxis(Mathf.Round(angle), Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);

        rigid.velocity = relativePos * speed;

        if ((target - transform.position).sqrMagnitude < minDistanceNextIndex)
        {
            if (currIndex <= 0)
            {
                ChangeDir(1);
            }
            else if (currIndex >= GameController.instance.allCenterPoints.Count-1)
            {
                ChangeDir(-1);
            }

            currIndex += dir;

            //Debug.Log("Goto Index " + currIndex + " " + currLine + " Dir " + dir);
        }
    }

    public void ChangeLine(int offset)
    {
        if ((currLine == LinePlace.LEFT && offset == -1*dir) ||
            (currLine == LinePlace.RIGHT && offset == 1*dir))
        {
            return;
        }

        currLine += offset*dir;
        currIndex += dir;
        //Handheld.Vibrate();

        //prevent error when out of range
        if (dir == 1)
        {
            currIndex = Mathf.Min(currIndex, GameController.instance.allCenterPoints.Count - 1);
        }
        else if(dir == -1)
        {
            currIndex = Mathf.Max(currIndex, 0);
        }
    }

    public void ChangeDir()
    {
        //Handheld.Vibrate();
        //dir *= -1;
        ChangeDir(dir * -1);
    }

    public void ChangeDir(int state)
    {
        //Handheld.Vibrate();
        dir = state;
        EndAllTrail();
        CreateAllTrail();
    }

    void CreateAllTrail()
    {
        trailEmitter.NewTrail();
        if (activeOutLine)
        {
            trailEmitterRight.NewTrail();
            trailEmitterLeft.NewTrail();
        }
    }

    void EndAllTrail()
    {
        trailEmitter.EndTrail();
        if (activeOutLine)
        {
            trailEmitterRight.EndTrail();
            trailEmitterLeft.EndTrail();
        }
    }

    private void OnDrawGizmos()
    {
        //if (GameController.instance != null &&
        //    currIndex < GameController.instance.allCenterPoints.Count &&
        //    GameController.instance.allCenterPoints[currIndex] != null)
        //{
        //    Gizmos.color = Color.green;
        //    Gizmos.DrawRay(transform.position, (GameController.instance.allCenterPoints[currIndex] - transform.position).normalized * 10);
        //}
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Contains("Obstacle") && !isDie)
        {
            isDie = true;
            GameController.instance.ChangeState(GameState.GAMEOVER);
        }
    }

}
